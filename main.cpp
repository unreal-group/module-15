#include <iostream>

void iterateAndPrint(int start, int end) {
    for (int i = start; i <= end; i += 2) {
        std::cout << i << " ";
    }
}

int main() {
    static int end = 50;

    iterateAndPrint(1, end);

    std::cout << std::endl << std::endl;

    iterateAndPrint(2, end);

    return 0;
}
